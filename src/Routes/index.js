import home from '../Components/Home/home.jsx';
import ContactIndex from '../Components/Contact/index.jsx';
import SearchContact from '../Components/Contact/searchContact.jsx';
import CreateContact from '../Components/Contact/Create/createContact.jsx';
import ViewTasks from '../Components/Task/viewTasks.jsx';
import CreateTask from '../Components/Task/createTask.jsx';

export {
    home,
    ContactIndex,
    SearchContact,
    CreateContact,
    ViewTasks,
    CreateTask
};