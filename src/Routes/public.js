import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from '../Components/Login/login';

const PublicRoutes = () => {
    return (<Fragment>
        <Switch>
            <Route exact path="/" component={Login}></Route>
        </Switch>
    </Fragment>)
}

export default PublicRoutes;