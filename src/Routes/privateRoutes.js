const RulesPages = {
    pages: {
        static: [
            { url: '/', component: "home" },
            { url: '/Contacts', component: "ContactIndex" },
            { url: '/searchContact', component: "SearchContact" },
            { url: '/createContact', component: "CreateContact" },
            { url: '/viewTasks', component: "ViewTasks" },
            { url: '/createTask', component: "CreateTask" },
        ]
    },
    sections: {
        policy: {
            billing: 'policy_billing',
            houseHold: 'policy_household',
            summary: 'policy_summary',
            endorsement: 'policy_endorsements',
            cancelReinstate: 'policy_cancel_reinstate',
            renewal: 'policy_renewal',
            notes: 'policy_notes',
            documents: 'polixy_documents',
        },
        tickets: {
            addTicket: 'add_ticket',
        },
        slugs: [
            'policy_billing',
            'policy_household',
            'policy_summary',
            'policy_endorsements',
            'policy_cancel_reinstate',
            'policy_renewal',
            'policy_notes',
            'polixy_documents',
            'add_ticket',
        ]
    },
};

export default RulesPages;