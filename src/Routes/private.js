import React, { Fragment, useEffect, useState } from 'react';
import { Switch, Route } from 'react-router-dom';
import auth from '../Components/Login/auth';
import * as Routes from './index'
import RulesPages from './privateRoutes';
import notFound from '../Components/Error/notFound.jsx';

const PrivateRoutes = (props) => {
    const [state, setState] = useState([]);
    useEffect(() => {

        if (auth.isAuthenticated()) {
            let newPages = [];
            let _pages = RulesPages.pages.static;
            for (let i = 0; i < _pages.length; i++) {
                newPages.push(_pages[i]);
            }

            setState(newPages);
        } else {
            props.history.push('/');
        }

        //temp bypass
        // let newPages = [];
        // let _pages = RulesPages.pages.static;
        // for (let i = 0; i < _pages.length; i++) {
        //     newPages.push(_pages[i]);
        // }
        // setState(newPages);
    }, [props]);
    return (
        <Fragment>
            <Switch>
                {
                    state.map((route, index) => (
                        <Route
                            exact
                            key={route.url}
                            component={Routes[route.component]}
                            path={route.url}
                        />
                    ))
                }
                <Route path="*" component={notFound} />
            </Switch>
        </Fragment>
    )
}

export default PrivateRoutes;
