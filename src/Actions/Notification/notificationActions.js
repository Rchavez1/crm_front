import { SET_FALSE_NOTIFICATION } from "../../Types/Notification";

export const SetFalseNotification = (task) => {
    return async (dispatch) => {
        dispatch({
            type: SET_FALSE_NOTIFICATION
        });
    };
};