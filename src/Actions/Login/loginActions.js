import {
    INIT_SESSION,
    SESSION_INIT_SUCCESSFUL,
    SESSION_INIT_ERROR,
    IS_SIGNED,
    USER_INIT
}
    from '../../Types/Login';

import Swal from 'sweetalert2';
import { PostLogin } from '../../Services/apiRequests';
//import { PostLogin } from '../../services';

export function isSignedAction() {
    return async (dispatch) => {
        var token = localStorage.getItem("token");
        var flag = false;

        if (token) {
            flag = true;
        }

        dispatch(isSigned(flag));
    }
}


const isSigned = (signed) => ({
    type: IS_SIGNED,
    payload: signed
});


export function loginAction(user) {
    return async (dispatch) => {
        debugger
        dispatch(initSession());

        //user.application = "PAC";

        try {
            var response = await PostLogin('Authentication/encrypted_token', user);
            var data = response.data.data;
            // var data = {
            //     token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
            //     fullName: 'Roberto',
            //     email: 'rchavez@endurancesys.net'
            // }
            localStorage.setItem("token", data.token);
            localStorage.setItem("fullname", data.fullname);
            localStorage.setItem("email", data.email);
            localStorage.setItem("roles", data.roles);

            dispatch(loginSuccessAction(data.fullname, data.email, data.token));

        }
        catch (err) {
            console.log(err)
            loginErrorAction()
            Swal.fire({

            })
        }
    }
}

const initSession = () => ({
    type: INIT_SESSION
});

export function loginSuccessAction(fullname, email, token) {
    return async (dispatch) => {
        var user = {
            fullname,
            email,
            token
        }

        await dispatch(loginSuccessfull(user));
    }
}

const loginSuccessfull = (user) => ({
    type: SESSION_INIT_SUCCESSFUL,
    payload: user
});

export function loginErrorAction() {
    return async (dispatch) => {
        dispatch(activeLoginError());
    }
}

const activeLoginError = () => ({
    type: SESSION_INIT_ERROR,
    payload: true
})

export function InitUserAction(usr) {
    return async (dispatch) => {
        let userData = usr.user ? usr.user[0] : usr.addUpdateUser;
        dispatch(InitUser(userData));
    }
}

const InitUser = (usr) => ({
    type: USER_INIT,
    payload: usr
})