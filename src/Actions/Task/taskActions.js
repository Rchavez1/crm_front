import { TaskSharp } from "@material-ui/icons";
import { Get, Post, Update } from "../../Services/apiRequests";
import { SET_SUCCESS } from "../../Types/Notification";
import { GET_TASKS, SAVE_NEW_TASK, SET_INITIAL_TASK_STATE, SET_TASK_ACTIVITY_STATE, SET_TASK_SELECTED, SET_TASK_STATE, TASK_MODAL_INFO_OPEN } from "../../Types/Task";

export function taskModalInfoOpen(open) {
    return async (dispatch) => {
        dispatch({
            type: TASK_MODAL_INFO_OPEN,
            payload: open
        });
    };
}

export function setTaskSelectedAction(data) {
    return async (dispatch) => {
        dispatch({
            type: SET_TASK_SELECTED,
            payload: data
        });
    };
}

export function SaveNewTaskAction(data) {
    return async (dispatch) => {
        function onSuccess(success) {
            return success;
        }
        function onError(error) {
            return error;
        }
        try {
            var response = await Post(`Task`, data);
            if (response.status === 201) {
                //set saved success
                dispatch({
                    type: SET_INITIAL_TASK_STATE
                });
            }

            return onSuccess(true);
        } catch (err) {
            return onError(err);
        }
    };
}

export const SetTaskActivityStateAction = (activity) => {
    return async (dispatch) => {
        dispatch({
            type: SET_TASK_ACTIVITY_STATE,
            payload: activity
        });
    };
};

export const SetTaskStateAction = (task) => {
    return async (dispatch) => {
        dispatch({
            type: SET_TASK_STATE,
            payload: task
        });
    };
};

export const SetInitialTaskStateAction = (task) => {
    return async (dispatch) => {
        dispatch({
            type: SET_INITIAL_TASK_STATE
        });
    };
};

export const GetAllTasksAction = () => {
    return async (dispatch) => {
        function onSuccess(success) {
            return success;
        }
        function onError(error) {
            return error;
        }
        try {
            var response = await Get(`task`);
            dispatch({
                type: GET_TASKS,
                payload: response.data
            });
            return onSuccess(true);
        } catch (err) {
            return onError(err);
        }
    };
};
export function UpdateTaskAction(task) {
    return async (dispatch) => {
        function onSuccess(success) {
            return success;
        }
        function onError(error) {
            return error;
        }
        try {
            var response = await Update(`task/${task.id}`, task);
            if (response.status === 204) {
                dispatch({
                    type: SET_SUCCESS,
                    payload: true
                });
                dispatch({
                    type: TASK_MODAL_INFO_OPEN,
                    payload: false
                })
            }
            return onSuccess(true);
        } catch (err) {
            return onError(err);
        }
    };
}