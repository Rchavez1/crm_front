import { Delete, Get, Post, Update } from "../../Services/apiRequests";
import { CREATE_CONTACT_ADDITIONAL_INFO, CREATE_CONTACT_ADDRESS, CREATE_CONTACT_SAVE, GET_ALL_CONTACTS, REMOVE_CONTACT_LOCKED, SAVE_CONTACT_ADDITIONAL_INFO, SEARCH_CONTACT_SELECTED, SET_CONTACT_EDIT_MODE, SET_CONTACT_INFO_STATE, SET_CONTACT_LOCKED, SET_EMPTY_CONTACT, UPDATE_CONTACT_ADDITIONAL_INFO, UPDATE_CONTACT_GENERAL_INFO } from "../../Types/Contact";

export function createContactSaveAction(contact) {
    return async (dispatch) => {
        function onSuccess(success) {
            return success;
        }
        function onError(error) {
            return error;
        }
        try {
            var response = await Post(`Contact`, contact);
            if (response.status === 201) {
                dispatch({
                    type: CREATE_CONTACT_SAVE,
                    payload: response.data
                });
                return onSuccess(true);
            }


        } catch (err) {
            return onError(err);
        }

    };
}

export function SearchContactSelectedAction(data) {
    return async (dispatch) => {
        dispatch({
            type: SEARCH_CONTACT_SELECTED,
            payload: data
        });
    };
}

export function SetContactEditModeAction(value) {
    return async (dispatch) => {
        dispatch({
            type: SET_CONTACT_EDIT_MODE,
            payload: false
        });
    }
}

export const GetContactsAction = (companyId) => {
    return async (dispatch) => {
        function onSuccess(success) {
            return success;
        }
        function onError(error) {
            return error;
        }
        try {
            var response = await Get(`contact`);
            dispatch({
                type: GET_ALL_CONTACTS,
                payload: response.data
            });
            return onSuccess(true);
        } catch (err) {
            return onError(err);
        }
    };
};

export const SetContactLockedAction = (contact) => {
    return async (dispatch) => {
        function onSuccess(success) {
            return success;
        }
        function onError(error) {
            return error;
        }
        try {
            var response = await Post(`LockedItem`, { user: localStorage.getItem("email"), itemId: contact.id });
            if (response.status === 201) {
                let contactLockedUpdated = contact;
                contactLockedUpdated.lockedItem = response.data
                dispatch({
                    type: SET_CONTACT_EDIT_MODE,
                    payload: true
                });
                dispatch({
                    type: SET_CONTACT_LOCKED,
                    payload: response.data
                });
            }

            return onSuccess(true);
        } catch (err) {
            return onError(err);
        }
    };
};

export const RemoveContactLockAction = (item) => {
    return async (dispatch) => {
        function onSuccess(success) {
            return success;
        }
        function onError(error) {
            return error;
        }
        try {
            var response = await Delete(`LockedItem`, { data: item });
            dispatch({
                type: REMOVE_CONTACT_LOCKED,
                payload: response.data
            });
            dispatch({
                type: SEARCH_CONTACT_SELECTED,
                payload: null
            });
            return onSuccess(true);
        } catch (err) {
            return onError(err);
        }
    };
};

export function createContactAddressAction(address) {
    return async (dispatch) => {
        function onSuccess(success) {
            return success;
        }
        function onError(error) {
            return error;
        }
        try {
            var response = await Post(`ContactAddress`, address);
            if (response.status === 201) {
                dispatch({
                    type: CREATE_CONTACT_ADDRESS,
                    payload: response.data
                });
            }

            return onSuccess(true);
        } catch (err) {
            return onError(err);
        }

    };
}

export function SetEmptyContactAction() {
    return async (dispatch) => {
        function onSuccess(success) {
            return success;
        }
        function onError(error) {
            return error;
        }
        try {
            dispatch({
                type: SET_EMPTY_CONTACT
            });

            return onSuccess(true);
        } catch (err) {
            return onError(err);
        }

    };
}

export function SetContactInfoStateAction(contact) {
    return async (dispatch) => {
        function onSuccess(success) {
            return success;
        }
        function onError(error) {
            return error;
        }
        try {
            dispatch({
                type: SET_CONTACT_INFO_STATE,
                payload: contact
            });

            return onSuccess(true);
        } catch (err) {
            return onError(err);
        }

    };
}

export function createContactAdditionalInfoAction(additionalInfo) {
    return async (dispatch) => {
        function onSuccess(success) {
            return success;
        }
        function onError(error) {
            return error;
        }
        try {
            dispatch({
                type: CREATE_CONTACT_ADDITIONAL_INFO,
                payload: additionalInfo
            });

            return onSuccess(true);
        } catch (err) {
            return onError(err);
        }

    };
}

export function SaveContactAdditionalInfoAction(additionalInfo) {
    return async (dispatch) => {
        function onSuccess(success) {
            return success;
        }
        function onError(error) {
            return error;
        }
        try {
            var response = await Post(`ContactAdditionalInfo`, additionalInfo);
            if (response.status === 201) {
                dispatch({
                    type: SAVE_CONTACT_ADDITIONAL_INFO,
                    payload: response.data
                });
            }
            return onSuccess(true);
        } catch (err) {
            return onError(err);
        }
    };
}

export function UpdateContactGeneralInfoAction(contact) {
    return async (dispatch) => {
        function onSuccess(success) {
            return success;
        }
        function onError(error) {
            return error;
        }
        try {
            var response = await Update(`Contact/${contact.id}`, contact);
            if (response.status === 201) {
                // dispatch({
                //     type: UPDATE_CONTACT_GENERAL_INFO,
                //     payload: response.data
                // });
            }
            return onSuccess(true);
        } catch (err) {
            return onError(err);
        }
    };
}

export function UpdateContactAdditionalInfoAction(contact) {
    return async (dispatch) => {
        function onSuccess(success) {
            return success;
        }
        function onError(error) {
            return error;
        }
        try {
            var response = await Update(`Contact/${contact.id}`, contact);
            if (response.status === 201) {
                // dispatch({
                //     type: UPDATE_CONTACT_GENERAL_INFO,
                //     payload: response.data
                // });
            }
            return onSuccess(true);
        } catch (err) {
            return onError(err);
        }
    };
}

export function SetUpdateContactAdditionalInfoAction(additionalInfo) {
    return async (dispatch) => {
        dispatch({
            type: UPDATE_CONTACT_ADDITIONAL_INFO,
            payload: additionalInfo
        });
    };
}