import axios from 'axios';
const BASE_URL = 'https://localhost:44388/api/';

export async function PostLogin(url = '', params = {}) {
    var request = await axios.post('https://ghawkidentity.azurewebsites.net/api/' + url, params, {
        headers: {
            Authorization: 'Bearer ' + localStorage.getItem('token')
        }
    });

    return request;
}

export function Get(url = '') {
    try {
        var request = axios.get(BASE_URL + url, {
            headers: {
                "Access-Control-Allow-Origin": 'http://localhost:3000',
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        });
    } catch (error) { }
    return request;
}
export async function Post(url = '', data = null) {
    try {
        var request = await axios.post(BASE_URL + url, data, {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        });
    } catch (error) {
        return error;
    }
    return request;
}
export async function Delete(url = '', data = null) {
    try {
        var request = await axios.delete(BASE_URL + url, data, {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        });
    } catch (error) {
        return error;
    }
    return request;
}

export async function Update(url = '', data = null) {
    try {
        var request = await axios.put(BASE_URL + url, data, {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        });
    } catch (error) {
        return error;
    }
    return request;
}