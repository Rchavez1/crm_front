import React, { Fragment } from 'react';
import { useState } from 'react';
import { DateRangePicker } from 'react-date-range';
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file

const DateRangePickerShared = ({ object, setDates }) => {
    //const initials ======================================================================================================

    //state redux variables ======================================================================================================

    //local constants ======================================================================================================
    const [selectionRange, setSelectionRange] = useState(
        [
            {
                startDate: new Date(),
                endDate: new Date(),
                key: 'selection',
            }
        ]
    )
    //functions ======================================================================================================

    //handles (react-dom) ======================================================================================================
    const handleSelect = (e) => {
        setSelectionRange([e.selection]);
        setDates({
            ...object,
            start: e.selection.startDate,
            end: e.selection.endDate
        })
    }
    //effects ======================================================================================================
    return (
        <Fragment>
            <DateRangePicker
                ranges={selectionRange}
                onChange={handleSelect}
            />
        </Fragment>
    );
}

export default DateRangePickerShared;