import * as React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import NoteAddIcon from '@material-ui/icons/NoteAdd';
import { useHistory } from 'react-router-dom';
import '../../uiStyle.css'

export default function CreateTaskMenu() {
    const history = useHistory();

    const onCardClick = () => {
        history.push('/createTask');
    }
    return (
        <Card className="cardHover" sx={{ maxWidth: 110 }} onClick={onCardClick}>
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', padding: 10, paddingBottom: 0 }}>
                <NoteAddIcon fontSize="medium" color="primary" />
            </div>
            <CardContent style={{ padding: 10, textAlign: 'center' }}>
                <Typography component="div">
                    CreateTask
                </Typography>
            </CardContent>
        </Card>
    );
}
