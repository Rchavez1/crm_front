import * as React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import { useHistory } from 'react-router-dom';
import '../../uiStyle.css'

export default function CreateContactMenu() {
    const history = useHistory();

    const onCardClick = () => {
        history.push('/createContact');
    }
    return (
        <Card className="cardHover" sx={{ maxWidth: 110 }} onClick={onCardClick}>
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', padding: 10, paddingBottom: 0 }}>
                <PersonAddIcon fontSize="medium" color="primary" />
            </div>
            <CardContent style={{ padding: 10, textAlign: 'center' }}>
                <Typography component="div">
                    Create Contact
                </Typography>
            </CardContent>
        </Card>
    );
}
