import styled from '@emotion/styled';
import { Divider, FormControl, Grid, InputLabel, MenuItem, Select, TextField } from '@material-ui/core';
import moment from 'moment';
import React, { Fragment } from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SearchContactSelectedAction, SetContactEditModeAction } from '../../../Actions/Contact/contactActions';

const TextFieldStyled = styled(TextField)({

    marginBottom: '20px',
    width: '100%',
    backgroundColor: 'white'
});
const EditContactGeneralInfo = () => {
    //const initials ======================================================================================================
    const dispatch = useDispatch();
    //state redux variables ======================================================================================================
    const editContactState = useSelector(c => c.contact.searchContactSelected);
    const editContactModeState = useSelector(c => c.contact.editMode);

    //local constants ======================================================================================================
    const [contactGeneralInfo, setContactGeneralInfo] = useState(editContactState)
    //functions ======================================================================================================

    //handles (react-dom) ======================================================================================================
    const onContactGeneralInfoChange = (e) => {
        setContactGeneralInfo({
            ...contactGeneralInfo,
            [e.target.name]: e.target.value
        })
    }

    const onBlurContact = (e) => {
        //dispatch edited object to searchContactSelected

        dispatch(SearchContactSelectedAction(contactGeneralInfo))
    }
    //effects ======================================================================================================
    useEffect(() => {
        setContactGeneralInfo(editContactState)
    }, [editContactState])
    useEffect(() => {
        dispatch(SetContactEditModeAction(false))
    }, [])
    return (
        <Fragment>
            <Grid container>
                <Grid item xs={6} md={6} lg={6}>
                    <FormControl variant="outlined" style={{ width: '100%', marginRight: '10px', marginBottom: '10px', backgroundColor: 'white' }}>
                        <InputLabel>Type</InputLabel>
                        <Select
                            id="type"
                            value={contactGeneralInfo.type}
                            onChange={onContactGeneralInfoChange}
                            label="Type"
                            name="type"
                            onBlur={onBlurContact}
                            readOnly={!editContactModeState}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            <MenuItem value={"Mr"}>Mr</MenuItem>
                            <MenuItem value={"Ms"}>Ms</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12}>
                    <Divider />
                    <br />
                </Grid>
                <Grid item xs={12} md={2} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="title" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="title" label="Title" defaultValue="" value={contactGeneralInfo.title || ''} InputProps={{ readOnly: !editContactModeState }} />
                </Grid>
                <Grid item xs={12} md={3} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="firstName" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="firstName" label="First Name" value={contactGeneralInfo.firstName || ''} InputProps={{ readOnly: !editContactModeState }} />
                </Grid>
                <Grid item xs={12} md={3} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="middleName" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="middleName" label="Middle Name" value={contactGeneralInfo.middleName || ''} InputProps={{ readOnly: !editContactModeState }} />
                </Grid>
                <Grid item xs={12} md={3} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="lastName" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="lastName" label="Last Name" value={contactGeneralInfo.lastName || ''} InputProps={{ readOnly: !editContactModeState }} />
                </Grid>
                <Grid item xs={12} md={3} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="dob" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="dob" label="DoB" type="date" style={{ width: '100%' }} value={moment(new Date(contactGeneralInfo.doB)).format('YYYY-MM-DD')} InputProps={{ readOnly: !editContactModeState }} />
                </Grid>
                <Grid item xs={12} md={4} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="company" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="company" label="Company" value={contactGeneralInfo.company || ''} InputProps={{ readOnly: !editContactModeState }} />
                </Grid>
                <Grid item xs={12} md={3} style={{ paddingRight: '20px' }}>
                    <FormControl variant="outlined" style={{ width: '100%', marginBottom: '10px', backgroundColor: 'white' }}>
                        <InputLabel>Nationality</InputLabel>
                        <Select
                            id="nationality"
                            value={contactGeneralInfo.nationality}
                            onChange={onContactGeneralInfoChange}
                            label="Nationality"
                            name="nationality"
                            value={contactGeneralInfo.nationality}
                            onBlur={onBlurContact}
                            readOnly={!editContactModeState}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            <MenuItem value={10}>Ten</MenuItem>
                            <MenuItem value={20}>Twenty</MenuItem>
                            <MenuItem value={30}>Thirty</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12} md={3} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="language" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="language" label="Language" value={contactGeneralInfo.language || ''} InputProps={{ readOnly: !editContactModeState }} />
                </Grid>
                <Grid item xs={12} md={3} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="department" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="department" label="Department" value={contactGeneralInfo.department || ''} InputProps={{ readOnly: !editContactModeState }} />
                </Grid>
                <Grid item xs={12} md={3} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="weekDays" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="weekDays" label="Week Days" value={contactGeneralInfo.weekDays || ''} InputProps={{ readOnly: !editContactModeState }} />
                </Grid>
                <Grid item xs={12} md={3} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="rangeHours" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="rangeHours" label="Range Hours" value={contactGeneralInfo.rangeHours || ''} InputProps={{ readOnly: !editContactModeState }} />
                </Grid>
                <Grid item xs={12}>
                    <Divider />
                    <br />
                </Grid>
            </Grid>
        </Fragment>
    );
}

export default EditContactGeneralInfo;