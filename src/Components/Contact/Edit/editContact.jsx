import { Alert, AppBar, Box, Fab, Snackbar, Tab, Tabs, Typography, useTheme } from '@material-ui/core';
import PropTypes from 'prop-types';
import React, { Fragment } from 'react';
import SwipeableViews from 'react-swipeable-views';
import { useState } from 'react';
import CreateContactContactInfo from '../Create/createContactContactInfo';
import CreateContactAdditionalInfo from '../Create/createContactAdditionalInfo';
import SaveIcon from '@material-ui/icons/Save';
import { useDispatch, useSelector } from 'react-redux';
import { GetContactsAction, RemoveContactLockAction, SearchContactSelectedAction, SetContactEditModeAction, SetContactLockedAction, UpdateContactAdditionalInfoAction, UpdateContactGeneralInfoAction } from '../../../Actions/Contact/contactActions';
import EditContactGeneralInfo from './editContactGeneralInfo';
import { HubConnectionBuilder } from '@microsoft/signalr';
import { useEffect } from 'react';
import CancelIcon from '@material-ui/icons/Cancel';
import EditContactContactInfo from './editContactContactInfo';
import EditIcon from '@material-ui/icons/Edit';
import EditContactAdditionalInfo from './editContactAdditionalInfo';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}


const EditContact = () => {
    //const initials ======================================================================================================
    const theme = useTheme();
    const dispatch = useDispatch();

    //state redux variables ======================================================================================================
    const editContactState = useSelector(c => c.contact.searchContactSelected);
    const editContactModeState = useSelector(c => c.contact.editMode);

    //local constants ======================================================================================================
    const [value, setValue] = useState(0);
    const [connection, setConnection] = useState(null);
    const [editMode, setEditMode] = useState(editContactModeState);
    const [openSavedMessage, setOpenSavedMessage] = useState(false);

    //functions ======================================================================================================

    //handles (react-dom) ======================================================================================================
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };

    const onSaveContactClick = async () => {
        switch (value) {
            case 0:
                let updateResponse = await dispatch(UpdateContactGeneralInfoAction(editContactState));
                if (updateResponse) {
                    setOpenSavedMessage(true)
                }
                break;
            case 2:
                let updateAdditionalInfoResponse = await dispatch(UpdateContactAdditionalInfoAction(editContactState));
                if (updateAdditionalInfoResponse) {
                    setOpenSavedMessage(true)
                }
                break;
            default:
                break;
        }

    }

    const onCloseContact = () => {
        if (editContactState.lockedItem) {
            if (editContactState.lockedItem.user === localStorage.getItem('email')) {
                dispatch(RemoveContactLockAction(editContactState.lockedItem))
            }
            else {
                dispatch(SearchContactSelectedAction(null))
            }
        }
        else {
            dispatch(SearchContactSelectedAction(null))
        }
        dispatch(GetContactsAction())
    }

    const onEditModeClick = () => {
        dispatch(SetContactLockedAction(editContactState))
    }

    //effects ======================================================================================================
    useEffect(() => {
        const newConnection = new HubConnectionBuilder()
            .withUrl('https://localhost:44388/signalr/hubs/status')
            .withAutomaticReconnect()
            .build();

        setConnection(newConnection);
    }, []);
    useEffect(() => {
        if (connection) {
            connection.start()
                .then(result => {
                    console.log('Connected!');

                    connection.on('ReceiveMessage', message => {
                        console.log(message)
                    });
                })
                .catch(e => console.log('Connection failed: ', e));
        }
    }, [connection]);
    return (
        <Fragment>
            <AppBar position="static" color="default">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="fullWidth"
                    aria-label="full width tabs example"
                >
                    <Tab label="General Info" {...a11yProps(0)} />
                    <Tab label="Contact Info" {...a11yProps(1)} />
                    <Tab label="Additional Info" {...a11yProps(2)} />
                </Tabs>
            </AppBar>
            <SwipeableViews
                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                index={value}
                onChangeIndex={handleChangeIndex}
            >
                <TabPanel value={value} index={0} dir={theme.direction}>
                    <EditContactGeneralInfo />
                </TabPanel>
                <TabPanel value={value} index={1} dir={theme.direction}>
                    <EditContactContactInfo />
                </TabPanel>
                <TabPanel value={value} index={2} dir={theme.direction}>
                    <EditContactAdditionalInfo />
                </TabPanel>
            </SwipeableViews>

            {editContactModeState ?
                <Fab disabled={editContactState.lockedItem ? editContactState.lockedItem.user === localStorage.getItem('email') ? false : true : false} variant="extended" style={{ position: 'absolute', right: '30px', bottom: '30px' }} onClick={onSaveContactClick}>
                    <SaveIcon />
                    Save
                </Fab>
                :
                <Fab disabled={editContactState.lockedItem ? editContactState.lockedItem.user === localStorage.getItem('email') ? false : true : false} variant="extended" style={{ position: 'absolute', right: '30px', bottom: '30px' }} onClick={onEditModeClick}>
                    <EditIcon />
                    Edit
                </Fab>
            }

            <Fab variant="extended" style={{ position: 'absolute', right: '150px', bottom: '30px' }} onClick={onCloseContact}>
                <CancelIcon />
                Close
            </Fab>
            {editContactState.lockedItem ?
                <div>
                    <Snackbar
                        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
                        open={editContactState.lockedItem ? true : false}
                    >
                        <Alert severity="warning">Contact Locked for edit by {editContactState.lockedItem.user}!</Alert>
                    </Snackbar>
                </div>
                :
                null
            }
            <div>
                <Snackbar
                    anchorOrigin={{ vertical: "bottom", horizontal: "left" }}
                    open={openSavedMessage}
                >
                    <Alert severity="success">Saved!</Alert>
                </Snackbar>
            </div>
        </Fragment>
    );
}

export default EditContact;