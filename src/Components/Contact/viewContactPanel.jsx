import { Paper } from '@material-ui/core';
import React, { Fragment } from 'react';
import EditContact from './Edit/editContact';

const ViewContactPanel = () => {
    //const initials ======================================================================================================

    //state redux variables ======================================================================================================

    //local constants ======================================================================================================

    //functions ======================================================================================================

    //handles (react-dom) ======================================================================================================

    //effects ======================================================================================================
    return (
        <Fragment>
            <Paper style={{ marginLeft: '25px', height: '100%', backgroundColor: '#e3ffd108' }}>
                <EditContact />
            </Paper>
        </Fragment>
    );
}

export default ViewContactPanel;