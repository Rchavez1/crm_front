import React from 'react';
import Modal from '@material-ui/core/Modal';
import { Button, Card, CardActions, CardContent, CardHeader, Divider, FormControl, Grid, IconButton, InputLabel, MenuItem, Select, TextField } from '@material-ui/core';
import styled from '@emotion/styled';
import { useState } from 'react';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import { v4 as uuidv4 } from 'uuid';
import { useDispatch, useSelector } from 'react-redux';
import { createContactAddressAction } from '../../Actions/Contact/contactActions';

const TextFieldStyled = styled(TextField)({
    marginRight: '10px',
    marginBottom: '10px'
});

function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
        position: 'absolute',
        width: '70%',
        backgroundColor: 'transparent'
    };
}

export default function AddContactModal({ open, setOpen, editModal }) {
    const dispatch = useDispatch();

    const [modalStyle] = React.useState(getModalStyle);

    const contactState = useSelector(state => state.contact.createContact);
    const contactEditState = useSelector(state => state.contact.searchContactSelected);

    const [addContactInfo, setAddContactInfo] = useState({
        contactId: editModal ? contactEditState.id : contactState.id,
        type: ""
    })
    const [phones, setPhones] = useState([
        {
            id: uuidv4(),
            phoneNumber: ""
        }
    ]);

    const [emails, setEmails] = useState([
        {
            id: uuidv4(),
            email: ""
        }
    ]);

    const handleClose = () => {
        setOpen(false)
    };

    const onContactInfoChange = (e) => {
        setAddContactInfo({
            ...addContactInfo,
            [e.target.name]: e.target.value
        })
    }

    const onPhoneChange = (e) => {
        var newPhonesArray = phones.map((phone) => {
            if (phone.id === e.target.id)
                return {
                    ...phone,
                    [e.target.name]: e.target.value
                }
            return phone;
        })
        setPhones(newPhonesArray)
    }

    const onAddPhoneClick = () => {
        setPhones([
            ...phones,
            {
                id: uuidv4(),
                phoneNumber: ""
            }
        ])
    }

    const onRemovePhoneClick = (e) => {
        let idtoRemove = e.target.parentElement.name;
        let newPhonesArray = phones.filter(p => p.id !== idtoRemove);
        debugger
        setPhones(newPhonesArray)
    }

    const onEmailChange = (e) => {
        var newEmailsArray = emails.map((email) => {
            if (email.id === e.target.id)
                return {
                    ...email,
                    [e.target.name]: e.target.value
                }
            return email;
        })
        setEmails(newEmailsArray)
    }

    const onAddEmailClick = () => {
        setEmails([
            ...emails,
            {
                id: uuidv4(),
                email: "",
            }
        ])
    }

    const onRemoveEmailClick = (e) => {
        let idtoRemove = e.target.parentElement.name;
        let newEmailsArray = emails.filter(p => p.id !== idtoRemove);
        setEmails(newEmailsArray)
    }

    const onSaveAddressClick = async (e) => {
        let addressObj = {
            ...addContactInfo,
            contactPhones: phones,
            contactEmails: emails
        }
        dispatch(createContactAddressAction(addressObj)).then((resp) => {
            console.log(resp)
            setOpen(false)
        })


    }

    return (
        <div>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <div style={modalStyle}>
                    <Card style={{ borderRadius: '10px' }}>
                        <CardHeader
                            title="Add contact Info"
                            action={
                                <IconButton onClick={handleClose} aria-label="settings" style={{ color: 'white' }}>
                                    <HighlightOffIcon fontSize="large" />
                                </IconButton>
                            }
                            style={{ backgroundColor: '#32B932', color: 'white' }}
                        />
                        <CardContent style={{ boxShadow: 'inset 0px 0px 6px 0px #8C8C8C' }}>
                            <Grid container>
                                <Grid item xs={12} md={12} lg={12}>
                                    <FormControl variant="outlined" style={{ width: '50%', marginRight: '10px', marginBottom: '10px' }}>
                                        <InputLabel>Type</InputLabel>
                                        <Select
                                            id="type"
                                            value={addContactInfo.type}
                                            onChange={onContactInfoChange}
                                            label="Type"
                                            name="type"
                                        >
                                            <MenuItem value="">
                                                <em>None</em>
                                            </MenuItem>
                                            <MenuItem value={"Home"}>Home</MenuItem>
                                            <MenuItem value={"Office"}>Office</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12}>
                                    <Divider />
                                    <br />
                                    <div style={{ fontWeight: 'bold', marginBottom: '10px', fontSize: '18px' }} color="primary">
                                        Address
                                    </div>
                                </Grid>
                                <Grid item sm={12} lg={5}>
                                    <TextFieldStyled id="address" label="Address" style={{ width: '97%' }}
                                        name="address"
                                        value={addContactInfo.address}
                                        onChange={onContactInfoChange}
                                    />
                                </Grid>
                                <Grid item sm={12} lg={2}>
                                    <TextFieldStyled id="zip" label="Zip"
                                        name="zip"
                                        value={addContactInfo.zip}
                                        onChange={onContactInfoChange}
                                    />
                                </Grid>
                                <Grid item sm={12} lg={1}>
                                    <TextFieldStyled id="state" label="State"
                                        name="state"
                                        value={addContactInfo.state}
                                        onChange={onContactInfoChange}
                                    />
                                </Grid>
                                <Grid item sm={12} lg={2}>
                                    <TextFieldStyled id="city" label="City"
                                        name="city"
                                        value={addContactInfo.city}
                                        onChange={onContactInfoChange}
                                    />
                                </Grid>
                                <Grid item sm={12} lg={2}>
                                    <TextFieldStyled id="country" label="Country"
                                        name="country"
                                        value={addContactInfo.country}
                                        onChange={onContactInfoChange}
                                    />
                                </Grid>
                                <Grid item sm={12} lg={2}>
                                    <TextFieldStyled id="category" label="Category"
                                        name="category"
                                        value={addContactInfo.category}
                                        onChange={onContactInfoChange}
                                    />
                                </Grid>
                                <Grid item sm={12} lg={2}>
                                    <TextFieldStyled id="tag" label="Tag"
                                        name="tag"
                                        value={addContactInfo.tag}
                                        onChange={onContactInfoChange}
                                    />
                                </Grid>
                            </Grid>
                            <Grid container>
                                <Grid item sm={12} md={6}>
                                    <Divider />
                                    <br />
                                    <div style={{ fontWeight: 'bold', marginBottom: '10px', fontSize: '18px' }} color="primary">
                                        Phones
                                    </div>
                                </Grid>
                                <Grid item sm={12} md={6}>
                                    <Divider />
                                    <br />
                                    <div style={{ fontWeight: 'bold', marginBottom: '10px', fontSize: '18px' }} color="primary">
                                        E-mails
                                    </div>
                                </Grid>
                            </Grid>
                            <Grid container>
                                <Grid item md={6}>
                                    {phones.map((phone, index) => {
                                        return (
                                            <Grid container>
                                                <Grid item xs={12} md={3} lg={3}>
                                                    <FormControl variant="outlined" style={{ width: '96%', marginRight: '10px', marginBottom: '10px' }}>
                                                        <InputLabel>Type</InputLabel>
                                                        <Select
                                                            id={phone.id}
                                                            value={phone.type}
                                                            onChange={onPhoneChange}
                                                            label="Phone Type"
                                                        >
                                                            <MenuItem value="">
                                                                <em>None</em>
                                                            </MenuItem>
                                                            <MenuItem value={10}>Ten</MenuItem>
                                                            <MenuItem value={20}>Twenty</MenuItem>
                                                            <MenuItem value={30}>Thirty</MenuItem>
                                                        </Select>
                                                    </FormControl>
                                                </Grid>
                                                <Grid item sm={12} lg={5}>
                                                    <TextFieldStyled onChange={onPhoneChange} name="phoneNumber" id={phone.id} label="Number" value={phone.phoneNumber} />
                                                </Grid>
                                                {index === 0 ?
                                                    <Grid item sm={1} lg={1}>
                                                        <IconButton onClick={onAddPhoneClick} aria-label="settings">
                                                            <AddCircleOutlineIcon color="primary" fontSize="large" />
                                                        </IconButton>
                                                    </Grid>
                                                    :
                                                    <Grid item sm={1} lg={1}>
                                                        <IconButton name={phone.id} onClick={onRemovePhoneClick} aria-label="settings">
                                                            <RemoveCircleOutlineIcon color="error" fontSize="large" />
                                                        </IconButton>
                                                    </Grid>
                                                }
                                            </Grid>
                                        )

                                    })
                                    }
                                </Grid>
                                <Grid item md={6}>
                                    {emails.map((email, index) => {
                                        return (
                                            <Grid container>
                                                <Grid item xs={12} md={3} lg={3}>
                                                    <FormControl variant="outlined" style={{ width: '96%', marginRight: '10px', marginBottom: '10px' }}>
                                                        <InputLabel>Type</InputLabel>
                                                        <Select
                                                            id={email.id}
                                                            value={email.type}
                                                            onChange={onEmailChange}
                                                            label="Email Type"
                                                        >
                                                            <MenuItem value="">
                                                                <em>None</em>
                                                            </MenuItem>
                                                            <MenuItem value={10}>Ten</MenuItem>
                                                            <MenuItem value={20}>Twenty</MenuItem>
                                                            <MenuItem value={30}>Thirty</MenuItem>
                                                        </Select>
                                                    </FormControl>
                                                </Grid>
                                                <Grid item sm={12} lg={5}>
                                                    <TextFieldStyled onChange={onEmailChange} name="email" id={email.id} label="Email" value={email.email} />
                                                </Grid>
                                                {index === 0 ?
                                                    <Grid item sm={1} lg={1}>
                                                        <IconButton onClick={onAddEmailClick} aria-label="settings">
                                                            <AddCircleOutlineIcon color="primary" fontSize="large" />
                                                        </IconButton>
                                                    </Grid>
                                                    :
                                                    <Grid item sm={1} lg={1}>
                                                        <IconButton name={email.id} onClick={onRemoveEmailClick} aria-label="settings">
                                                            <RemoveCircleOutlineIcon color="error" fontSize="large" />
                                                        </IconButton>
                                                    </Grid>
                                                }
                                            </Grid>
                                        )

                                    })
                                    }
                                </Grid>
                            </Grid>

                        </CardContent>
                        <CardActions style={{ flexDirection: 'row-reverse', backgroundColor: 'antiquewhite' }}>
                            <Button
                                variant="contained"
                                color="secondary"
                                startIcon={<SaveIcon />}
                                onClick={onSaveAddressClick}
                            >
                                Save
                            </Button>
                            <Button
                                variant="contained"
                                color="secondary"
                                startIcon={<CancelIcon />}
                                style={{ marginRight: '20px' }}
                            >
                                Cancel
                            </Button>
                        </CardActions>
                    </Card>

                </div>
            </Modal>
        </div>
    );
}
