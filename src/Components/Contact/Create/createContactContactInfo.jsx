import { Button, Grid, Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core';
import React, { Fragment } from 'react';
import { useTable } from 'react-table';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import AddContactModal from '../addContactInfoModal';
import { useState } from 'react';
import { useSelector } from 'react-redux';
import { useEffect } from 'react';

const CreateContactContactInfo = () => {
    //const initials ======================================================================================================

    //state redux variables ======================================================================================================
    const ContactState = useSelector(state => state.contact.createContact)
    //local constants ======================================================================================================
    const [data, setData] = useState(ContactState.contactAddress ? ContactState.contactAddress : []);
    const columns = React.useMemo(
        () => [
            {
                Header: 'Address',
                columns: [
                    {
                        Header: 'Type',
                        accessor: 'type',
                    },
                    {
                        Header: 'Address',
                        accessor: 'address',
                    },
                    {
                        Header: 'City',
                        accessor: 'city',
                    },
                    {
                        Header: 'State',
                        accessor: 'state',
                    },
                    {
                        Header: 'Zip',
                        accessor: 'zip',
                    },
                    {
                        Header: 'Country',
                        accessor: 'country',
                    },
                    {
                        Header: 'Phone',
                        accessor: 'phone',
                    },
                    {
                        Header: 'Email',
                        accessor: 'email',
                    },
                ],
            }
        ],
        []
    )
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
    } = useTable({ columns, data })

    const [addContactModalOpen, setAddContactModalOpen] = useState(false)
    //functions ======================================================================================================

    //handles (react-dom) ======================================================================================================
    const onAddClick = () => {
        setAddContactModalOpen(true)
    }
    //effects ======================================================================================================
    useEffect(() => {
        setData(ContactState.contactAddress ? ContactState.contactAddress : [])
    }, [ContactState.contactAddress])
    return (
        <Fragment>
            <Grid container>
                <Grid item xs={12}>
                    <Button
                        variant="contained"
                        color="secondary"
                        startIcon={<AddCircleOutlineIcon />}
                        style={{ marginBottom: '10px' }}
                        onClick={onAddClick}
                    >
                        Add Contact Info
                    </Button>
                </Grid>
                <Grid item xs={12}>
                    <Table {...getTableProps()}>
                        <TableHead>
                            {headerGroups.map((headerGroup, index) => {
                                let bColor = index === 1 ? 'black' : 'white'
                                let tColor = index === 1 ? 'white' : 'black'
                                switch (index) {
                                    case 1:
                                        return (
                                            <TableRow {...headerGroup.getHeaderGroupProps()} >
                                                {headerGroup.headers.map(column => (
                                                    <TableCell
                                                        style={{ backgroundColor: bColor, color: tColor }}
                                                        {...column.getHeaderProps()}
                                                    >
                                                        {column.render('Header')}
                                                    </TableCell>
                                                ))}
                                            </TableRow>
                                        )
                                    default:
                                        return null
                                }
                            })}
                        </TableHead>
                        <TableBody {...getTableBodyProps()}>
                            {rows.map((row) => {
                                prepareRow(row)
                                return (
                                    <TableRow
                                        {...row.getRowProps()}
                                        key={row.name}
                                    >
                                        {row.cells.map(cell => {
                                            return (
                                                <TableCell
                                                    {...cell.getCellProps()}
                                                >
                                                    {cell.render('Cell')}
                                                </TableCell>
                                            )
                                        })}
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </Grid>
            </Grid>
            <AddContactModal open={addContactModalOpen} setOpen={setAddContactModalOpen} />
        </Fragment>
    );
}

export default CreateContactContactInfo;