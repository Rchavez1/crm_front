import { Alert, AppBar, Box, Fab, Snackbar, Tab, Tabs, useTheme } from '@material-ui/core';
import PropTypes from 'prop-types';
import React, { Fragment } from 'react';
import Layout from '../../Layout/layout';
import { useState } from 'react';
import CreateContactGeneralInfo from './createContactGeneralInfo';
import CreateContactContactInfo from './createContactContactInfo';
import CreateContactAdditionalInfo from './createContactAdditionalInfo';
import SaveIcon from '@material-ui/icons/Save';
import { useDispatch, useSelector } from 'react-redux';
import { createContactAdditionalInfoAction, createContactSaveAction, SaveContactAdditionalInfoAction, SetEmptyContactAction } from '../../../Actions/Contact/contactActions';
import { useEffect } from 'react';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <div>{children}</div>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}


const CreateContact = () => {
    //const initials ======================================================================================================
    const theme = useTheme();
    const dispatch = useDispatch();

    //state redux variables ======================================================================================================
    const contactCreated = useSelector(c => c.contact.createContact);

    //local constants ======================================================================================================
    const [value, setValue] = useState(0);
    const [openSnack, setOpenSnack] = useState(false);
    const [snackType, setSnackType] = useState({ message: '', color: 'success' })

    //functions ======================================================================================================

    //handles (react-dom) ======================================================================================================
    const handleChange = (event, newValue) => {
        if (!contactCreated.id) {
            setSnackType({ message: 'Save contact to continue!', color: 'warning' })
            setOpenSnack(true)
        }
        else {
            setValue(newValue);
        }

    };

    const onSaveContactClick = async () => {
        switch (value) {
            case 0:
                let saved = dispatch(createContactSaveAction(contactCreated));
                saved.then((res) => {
                    console.log(res)
                    if (res) {
                        setSnackType({ message: 'Contact saved!', color: 'success' })
                    }
                    else {
                        setSnackType({ message: 'Error!', color: 'error' })
                    }
                    setOpenSnack(true)
                })
                break;
            case 2:
                let savedAdditional = dispatch(SaveContactAdditionalInfoAction(contactCreated.contactAdditionalInfo));
                savedAdditional.then((res) => {
                    console.log(res)
                    if (res) {
                        setSnackType({ message: 'Contact info saved!', color: 'success' })
                    }
                    else {
                        setSnackType({ message: 'Error!', color: 'error' })
                    }
                    setOpenSnack(true)
                })
                break;
            default:
                break;
        }

    }

    const closeSnack = () => {
        setOpenSnack(false)
    }


    //effects ======================================================================================================
    useEffect(() => {
        dispatch(SetEmptyContactAction())
    }, [])

    return (
        <Fragment>
            <Layout>
                <div>
                    <AppBar position="static" color="default">
                        <Tabs
                            value={value}
                            onChange={handleChange}
                            indicatorColor="primary"
                            textColor="primary"
                            variant="fullWidth"
                            aria-label="full width tabs example"
                        >
                            <Tab label="General Info" {...a11yProps(0)} />

                            <Tab label="Contact Info" {...a11yProps(1)} />
                            <Tab label="Additional Info" {...a11yProps(2)} />
                        </Tabs>
                    </AppBar>
                    <TabPanel value={value} index={0} dir={theme.direction}>
                        <CreateContactGeneralInfo />
                    </TabPanel>
                    <TabPanel value={value} index={1} dir={theme.direction}>
                        <CreateContactContactInfo />
                    </TabPanel>
                    <TabPanel value={value} index={2} dir={theme.direction}>
                        <CreateContactAdditionalInfo />
                    </TabPanel>
                </div>

                <Fab variant="extended" style={{ position: 'absolute', right: '30px', bottom: '30px', display: value === 1 ? 'none' : '' }} onClick={onSaveContactClick}>
                    <SaveIcon />
                    Save
                </Fab>
                <Snackbar anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }} open={openSnack} autoHideDuration={3000} onClose={closeSnack}>
                    <Alert severity={snackType.color}>
                        {snackType.message}
                    </Alert>
                </Snackbar>
            </Layout>
        </Fragment >
    );
}

export default CreateContact;