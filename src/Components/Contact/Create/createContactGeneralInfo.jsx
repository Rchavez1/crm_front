import styled from '@emotion/styled';
import { Divider, FormControl, Grid, InputLabel, MenuItem, Select, TextField } from '@material-ui/core';
import React, { Fragment } from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SetContactInfoStateAction } from '../../../Actions/Contact/contactActions';

const TextFieldStyled = styled(TextField)({
    width: '100%',
    marginBottom: '10px'
});
const CreateContactGeneralInfo = () => {
    //const initials ======================================================================================================
    const dispatch = useDispatch();
    //state redux variables ======================================================================================================
    const newContact = useSelector(c => c.contact.createContact);
    //local constants ======================================================================================================
    const [contactGeneralInfo, setContactGeneralInfo] = useState(newContact)
    //functions ======================================================================================================

    //handles (react-dom) ======================================================================================================
    const onContactGeneralInfoChange = (e) => {
        setContactGeneralInfo({
            ...contactGeneralInfo,
            [e.target.name]: e.target.value
        })
    }

    const onBlurContact = (e) => {
        dispatch(SetContactInfoStateAction(contactGeneralInfo))
    }
    //effects ======================================================================================================
    return (
        <Fragment>
            <Grid container>
                <Grid item xs={4} md={4} lg={4}>
                    <FormControl variant="outlined" style={{ width: '100%', marginRight: '10px', marginBottom: '10px' }}>
                        <InputLabel>Type</InputLabel>
                        <Select
                            id="type"
                            onChange={onContactGeneralInfoChange}
                            label="Type"
                            name="type"
                            value={contactGeneralInfo.type}
                            onBlur={onBlurContact}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            <MenuItem value={"Client"}>Client</MenuItem>
                            <MenuItem value={"Worker"}>Worker</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12}>
                    <Divider style={{ backgroundColor: '#32b932' }} />
                    <br />
                </Grid>
                <Grid item xs={12} md={1} style={{ paddingRight: '20px' }}>
                    <FormControl variant="outlined" style={{ width: '100%', marginRight: '10px', marginBottom: '10px' }}>
                        <InputLabel>Title</InputLabel>
                        <Select
                            id="title"
                            value={contactGeneralInfo.title}
                            onChange={onContactGeneralInfoChange}
                            label="Title"
                            name="title"
                            onBlur={onBlurContact}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            <MenuItem value={"Mr"}>Mr</MenuItem>
                            <MenuItem value={"Ms"}>Ms</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12} md={2} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="firstName" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="firstName" label="First Name" value={contactGeneralInfo.firstName} />
                </Grid>
                <Grid item xs={12} md={2} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="middleName" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="middleName" label="Middle Name" value={contactGeneralInfo.middleName} />
                </Grid>
                <Grid item xs={12} md={2} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="lastName" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="lastName" label="Last Name" value={contactGeneralInfo.lastName} />
                </Grid>
                <Grid item xs={12} md={2} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="dob" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="dob" label="DoB" type="date" style={{ width: '100%' }} value={contactGeneralInfo.dob} />
                </Grid>
                <Grid item xs={12}>
                    <Divider style={{ backgroundColor: '#32b932' }} />
                    <br />
                </Grid>
                <Grid item xs={12} md={2} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="company" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="company" label="Company" value={contactGeneralInfo.company} />
                </Grid>
                <Grid item xs={12} md={2} style={{ paddingRight: '20px' }}>
                    <FormControl variant="outlined" style={{ width: '100%', marginBottom: '10px' }}>
                        <InputLabel>Nationality</InputLabel>
                        <Select
                            id="nationality"
                            onChange={onContactGeneralInfoChange}
                            label="Nationality"
                            name="nationality"
                            value={contactGeneralInfo.nationality}
                            onBlur={onBlurContact}
                        >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            <MenuItem value={10}>Ten</MenuItem>
                            <MenuItem value={20}>Twenty</MenuItem>
                            <MenuItem value={30}>Thirty</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={12} md={2} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="language" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="language" label="Language" value={contactGeneralInfo.language} />
                </Grid>
                <Grid item xs={12} md={2} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="department" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="department" label="Department" value={contactGeneralInfo.department} />
                </Grid>
                <Grid item xs={12} md={2} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="weekDays" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="weekDays" label="Week Days" value={contactGeneralInfo.weekDays} />
                </Grid>
                <Grid item xs={12} md={2} style={{ paddingRight: '20px' }}>
                    <TextFieldStyled name="rangeHours" onChange={onContactGeneralInfoChange} onBlur={onBlurContact} id="rangeHours" label="Range Hours" value={contactGeneralInfo.rangeHours} />
                </Grid>
            </Grid>
        </Fragment>
    );
}

export default CreateContactGeneralInfo;