import { Divider, Grid, TextField } from '@material-ui/core';
import React, { Fragment, useCallback } from 'react';
import { useDropzone } from 'react-dropzone';
import styled from '@emotion/styled';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createContactAdditionalInfoAction } from '../../../Actions/Contact/contactActions';

const TextFieldStyled = styled(TextField)({
    marginRight: '10px',
    marginBottom: '10px'
});

const CreateContactAdditionalInfo = () => {
    //const initials ======================================================================================================
    const dispatch = useDispatch();
    //state redux variables ======================================================================================================
    const newContact = useSelector(c => c.contact.createContact);
    //local constants ======================================================================================================
    const [contactAdditionalInfo, setContactAdditionalInfo] = useState({
        remarks: '',
        websiteURL: '',
        facebookURL: '',
        instagramURL: '',
        contactId: newContact.id
    })
    //functions ======================================================================================================
    function MyDropzone() {
        const onDrop = useCallback((acceptedFiles) => {
            acceptedFiles.forEach((file) => {
                const reader = new FileReader()

                reader.onabort = () => console.log('file reading was aborted')
                reader.onerror = () => console.log('file reading has failed')
                reader.onload = () => {
                    // Do whatever you want with the file contents
                    const binaryStr = reader.result
                    console.log(binaryStr)
                }
                reader.readAsArrayBuffer(file)
            })

        }, [])
        const { getRootProps, getInputProps } = useDropzone({ onDrop })

        return (
            <div {...getRootProps()}>
                <input {...getInputProps()} />
                <p>Drag 'n' drop some files here, or click to select files</p>
            </div>
        )
    }
    //handles (react-dom) ======================================================================================================
    const handleContactAdditionalInfoChange = (e) => {
        setContactAdditionalInfo({
            ...contactAdditionalInfo,
            [e.target.name]: e.target.value
        })
    }
    const handleContactAdditionalInfoBlur = () => {
        dispatch(createContactAdditionalInfoAction(contactAdditionalInfo))
    }
    //effects ======================================================================================================
    return (
        <Fragment>
            <h3>
                Remarks
            </h3>
            <Divider />
            <br />
            <Grid container>
                <Grid item xs={12} md={12}>
                    <TextFieldStyled fullWidth id="remarks" label="Remarks" multiline rows={4}
                        onChange={handleContactAdditionalInfoChange}
                        onBlur={handleContactAdditionalInfoBlur}
                        name="remarks"
                        value={contactAdditionalInfo.remarks}
                    />
                </Grid>
            </Grid>
            <h3>
                Social Media
            </h3>
            <Divider />
            <br />
            <Grid container>
                <Grid item xs={12} md={2}>
                    <TextFieldStyled id="website" label="Website"
                        onChange={handleContactAdditionalInfoChange}
                        onBlur={handleContactAdditionalInfoBlur}
                        name="websiteURL"
                        value={contactAdditionalInfo.websiteURL}
                    />
                </Grid>
                <Grid item xs={12} md={2}>
                    <TextFieldStyled id="facebook" label="Facebook"
                        onChange={handleContactAdditionalInfoChange}
                        onBlur={handleContactAdditionalInfoBlur}
                        name="facebookURL"
                        value={contactAdditionalInfo.facebookURL}
                    />
                </Grid>
                <Grid item xs={12} md={2}>
                    <TextFieldStyled id="instagram" label="Instagram"
                        onChange={handleContactAdditionalInfoChange}
                        onBlur={handleContactAdditionalInfoBlur}
                        name="instagramURL"
                        value={contactAdditionalInfo.instagramURL}
                    />
                </Grid>
            </Grid>
            <h3>
                Files
            </h3>
            <Divider />
            <br />
            <Grid container>
                <Grid item xs={12} md={12}>
                    <MyDropzone />
                </Grid>
            </Grid>
        </Fragment>
    );
}

export default CreateContactAdditionalInfo;