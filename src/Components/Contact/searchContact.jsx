import { Grid, TableRow } from '@material-ui/core';
import { TableHead } from '@material-ui/core';
import { TableCell } from '@material-ui/core';
import { TextField } from '@material-ui/core';
import { TableBody } from '@material-ui/core';
import { Table } from '@material-ui/core';
import React, { Fragment } from 'react';
import { useMemo } from 'react';
import { useAsyncDebounce, useTable, useGlobalFilter } from 'react-table';
import Layout from '../Layout/layout';
import '../Contact/Style/style.css'
import { useState } from 'react';
import ViewContactPanel from './viewContactPanel';
import { useDispatch, useSelector } from 'react-redux';
import { GetContactsAction, SearchContactSelectedAction, SetContactLockedAction } from '../../Actions/Contact/contactActions';
import { useEffect } from 'react';

function GlobalFilter({ preGlobalFilteredRows, globalFilter, setGlobalFilter }) {
    const [value, setValue] = React.useState(globalFilter)
    const onChange = useAsyncDebounce(value => {
        setGlobalFilter(value || undefined)
    }, 200)

    return (
        <span>
            <div style={{ fontSize: '20px', fontWeight: 'bold' }}>Search:</div>
            <TextField
                variant="outlined"
                value={value || ""}
                onChange={e => {
                    setValue(e.target.value);
                    onChange(e.target.value);
                }}
                placeholder={``}
                style={{ width: '100%' }}
            />
        </span>
    )
}
const SearchContact = ({ theme }) => {
    //const initials ======================================================================================================
    const dispatch = useDispatch();
    //state redux variables ======================================================================================================
    const contactsState = useSelector(state => state.contact.contactsList);
    const searchContactState = useSelector(state => state.contact.searchContactSelected);
    //local constants ======================================================================================================
    const data = useMemo(
        () => contactsState,
        [contactsState]
    )
    const columns = React.useMemo(
        () => [
            {
                Header: 'Name',
                columns: [
                    {
                        Header: 'Name',
                        accessor: 'firstName',
                    },
                    {
                        Header: 'Middle Name',
                        accessor: 'middleName',
                    },
                    {
                        Header: 'Last Name',
                        accessor: 'lastName',
                    },
                ],
            },
            {
                Header: 'Info',
                columns: [
                    {
                        Header: 'Company',
                        accessor: 'company',
                    },
                    {
                        Header: 'State',
                        accessor: 'state',
                    },
                ],
            },
        ],
        []
    )
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        state,
        prepareRow,
        preGlobalFilteredRows,
        setGlobalFilter
    } = useTable({ columns, data }, useGlobalFilter)

    //functions ======================================================================================================

    //handles (react-dom) ======================================================================================================
    const onRowClick = (e) => {
        dispatch(SearchContactSelectedAction(e.original))
    }
    //effects ======================================================================================================
    useEffect(() => {
        dispatch(GetContactsAction())
    }, [])
    return (
        <Fragment>
            <Layout>
                <Grid container style={{ height: '100%' }}>
                    {searchContactState ?
                        null
                        :
                        <Grid item xs={12}>
                            <GlobalFilter
                                preGlobalFilteredRows={preGlobalFilteredRows}
                                globalFilter={state.globalFilter}
                                setGlobalFilter={setGlobalFilter}
                            />
                            <Table {...getTableProps()}>
                                <TableHead>
                                    {headerGroups.map((headerGroup, index) => {
                                        let bColor = index === 1 ? 'black' : 'white'
                                        let tColor = index === 1 ? 'white' : 'black'
                                        return (
                                            <TableRow {...headerGroup.getHeaderGroupProps()} >
                                                {headerGroup.headers.map(column => (
                                                    <TableCell
                                                        style={{ backgroundColor: bColor, color: tColor }}
                                                        {...column.getHeaderProps()}
                                                    >
                                                        {column.render('Header')}
                                                    </TableCell>
                                                ))}
                                            </TableRow>
                                        )
                                    })}
                                </TableHead>
                                <TableBody {...getTableBodyProps()} style={{ backgroundColor: '#6969690d' }}>
                                    {rows.map((row) => {
                                        prepareRow(row)
                                        return (
                                            <TableRow onClick={() => onRowClick(row)}
                                                {...row.getRowProps()}
                                                key={row.name}
                                            >
                                                {row.cells.map(cell => {
                                                    return (
                                                        <TableCell
                                                            {...cell.getCellProps()}
                                                        >
                                                            {cell.render('Cell')}
                                                        </TableCell>
                                                    )
                                                })}
                                            </TableRow>
                                        )
                                    })}
                                </TableBody>
                            </Table>
                        </Grid>
                    }
                    {searchContactState ?
                        <Grid item xs={12}>
                            <ViewContactPanel />
                        </Grid>
                        :
                        null
                    }

                </Grid>

            </Layout>
        </Fragment>
    );
}

export default SearchContact;