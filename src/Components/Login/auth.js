import jwtDecode from 'jwt-decode';

class Auth {
    constructor() {
        this.authenticated = localStorage.getItem('token') ? true : false;
    }

    setToken(token) {
        this.authenticated = true;
        localStorage.setItem('token', token);
    }


    isAuthenticated() {
        return this.authenticated;
        //temp bypass
        //return true;
    }

    getAccessSection(slug) {
        if (this.authenticated) {
            //let _decodeToken = this.decodeToken();
            //let isRole = _decodeToken.roles.find(item => item === 'Root');
            // let isRole = _decodeToken.name === 'John Doe' ? true : false;
            // let sections = _decodeToken.permissions.filter(item => item.type === 'Section');
            // if (isRole) {
            //     return false;
            // } else {
            //     let isDisable = true;
            //     let result = sections.find(item => item.slug === slug);
            //     if (result) {
            //         isDisable = false;
            //     }
            //     return isDisable;
            // }
        }
    }
}

export default new Auth();
