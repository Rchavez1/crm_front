import React, { Fragment, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {
    Typography,
    CircularProgress,
    FormControlLabel,
    Checkbox,
    Avatar,
    Link
} from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import auth from '../Login/auth.js';
import './login.css';
import { loginAction } from '../../Actions/Login/loginActions.js';


const Login = () => {

    //const initials ======================================================================================================
    const dispatch = useDispatch();

    //state redux variables ======================================================================================================
    const isSigned = useSelector(state => state.login.existingLogin);
    const loginLoading = useSelector(state => state.login.loginLoading);

    //local constants ======================================================================================================
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    //handles (react-dom) ======================================================================================================
    const onChangeEmail = e => {
        setEmail(e.target.value);
    }

    const onChangePassword = e => {
        setPassword(e.target.value);
    }

    const onLoginClick = () => {
        dispatch(loginAction({ "password": password, "userName": email }));
    }

    //effects ======================================================================================================


    return (
        <Fragment >
            <Grid container component="main">
                <Grid item xs={false} sm={12} md={7} component={Paper} elevation={0} />
                <Grid item xs={12} sm={null} md={5} component={Paper} elevation={6} className='loginPanel'>
                    <div>
                        <Avatar>
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Sign in
                        </Typography>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email"
                            type="email"
                            autoComplete="email"
                            name="email"
                            value={email}
                            onChange={onChangeEmail}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            value={password}
                            onChange={onChangePassword}
                        />
                        <FormControlLabel
                            control={<Checkbox value="remember" color="primary" />}
                            label="Remember me"
                        />
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            onClick={onLoginClick}
                        >
                            {false ? <CircularProgress /> : <div>Sign In</div>}
                        </Button>
                        <Grid container>
                            <Grid item xs>
                                <Link href="#" variant="body2">
                                    Forgot password?
                                </Link>
                            </Grid>
                        </Grid>
                    </div>
                </Grid>
            </Grid>
        </Fragment>
    );
}

export default Login;