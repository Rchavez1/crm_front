import React from 'react';
import Modal from '@material-ui/core/Modal';
import { Button, Card, CardActions, CardContent, CardHeader, Divider, FormControl, Grid, IconButton, InputLabel, MenuItem, Select, TextField } from '@material-ui/core';
import styled from '@emotion/styled';
import { useState } from 'react';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import RemoveCircleOutlineIcon from '@material-ui/icons/RemoveCircleOutline';
import { v4 as uuidv4 } from 'uuid';
import { useDispatch, useSelector } from 'react-redux';
import { createContactAddressAction } from '../../Actions/Contact/contactActions';
import { SetTaskActivityStateAction } from '../../Actions/Task/taskActions';

const TextFieldStyled = styled(TextField)({
    marginRight: '10px',
    marginBottom: '10px'
});

function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
        position: 'absolute',
        width: '70%',
        backgroundColor: 'transparent'
    };
}

export default function CreateTaskActivityModal({ open, setOpen }) {
    const dispatch = useDispatch();

    const [modalStyle] = useState(getModalStyle);


    const [taskActivity, setTaskActivity] = useState({
        title: '',
        description: '',
        finished: false
    })

    const handleClose = () => {
        setOpen(false)
    };

    const onTaskActivityChange = (e) => {
        setTaskActivity({
            ...taskActivity,
            [e.target.name]: e.target.value
        })
    }


    const onSaveActivityClick = async (e) => {
        dispatch(SetTaskActivityStateAction(taskActivity)).then((resp) => {
            setOpen(false)
        })
    }

    return (
        <div>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <div style={modalStyle}>
                    <Card style={{ borderRadius: '10px' }}>
                        <CardHeader
                            title="Task activity"
                            action={
                                <IconButton onClick={handleClose} aria-label="settings" style={{ color: 'white' }}>
                                    <HighlightOffIcon fontSize="large" />
                                </IconButton>
                            }
                            style={{ backgroundColor: '#32B932', color: 'white' }}
                        />
                        <CardContent style={{ boxShadow: 'inset 0px 0px 6px 0px #8C8C8C' }}>
                            <Grid container>
                                <Grid item sm={12} lg={3}>
                                    <TextFieldStyled id="title" label="Title" style={{ width: '97%' }}
                                        name="title"
                                        value={taskActivity.title}
                                        onChange={onTaskActivityChange}
                                    />
                                </Grid>
                                <Grid item sm={12} lg={8}>
                                    <TextFieldStyled id="description" label="Description"
                                        name="description"
                                        multiline
                                        rows={2}
                                        value={taskActivity.description}
                                        onChange={onTaskActivityChange}
                                        style={{ width: '97%' }}
                                    />
                                </Grid>
                            </Grid>
                        </CardContent>
                        <CardActions style={{ flexDirection: 'row-reverse', backgroundColor: 'antiquewhite' }}>
                            <Button
                                variant="contained"
                                color="secondary"
                                startIcon={<SaveIcon />}
                                onClick={onSaveActivityClick}
                            >
                                Save
                            </Button>
                            <Button
                                variant="contained"
                                color="secondary"
                                startIcon={<CancelIcon />}
                                style={{ marginRight: '20px' }}
                            >
                                Cancel
                            </Button>
                        </CardActions>
                    </Card>

                </div>
            </Modal>
        </div>
    );
}
