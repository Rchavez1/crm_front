import React, { Fragment } from 'react';
import { Calendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import "react-big-calendar/lib/css/react-big-calendar.css";
import { useDispatch, useSelector } from 'react-redux';
import { setTaskSelectedAction, taskModalInfoOpen } from '../../Actions/Task/taskActions';

function Event({ event }) {
    return (
        <div style={{ backgroundColor: event.priority === 'high' ? '#ff34349e' : '#0044ab85', width: '100%', borderRadius: '5px', padding: '2px 5px' }}>
            <strong>{event.title}</strong>
        </div>
    )
}

const TasksCalendar = () => {
    //const initials ======================================================================================================
    const localizer = momentLocalizer(moment)
    const dispatch = useDispatch();
    //state redux variables ======================================================================================================
    const taskListState = useSelector(state => state.task.taskList)
    //local constants ======================================================================================================
    const calendarEvents = {
        events: taskListState
    };
    //functions ======================================================================================================

    //handles (react-dom) ======================================================================================================
    const onDoubleClickTask = (e) => {
        dispatch(setTaskSelectedAction(e));
        dispatch(taskModalInfoOpen(true));

    }
    //effects ======================================================================================================
    return (
        <Fragment>
            <Calendar
                localizer={localizer}
                events={calendarEvents.events}
                startAccessor="start"
                endAccessor="end"
                style={{ height: '100%' }}
                onDoubleClickEvent={onDoubleClickTask}
                components={{
                    event: Event
                }}
            />

        </Fragment>
    );
}

export default TasksCalendar;