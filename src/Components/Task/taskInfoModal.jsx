import { Button, Card, CardActions, CardContent, CardHeader, Divider, Grid, IconButton, Modal, Step, StepButton, StepContent, StepLabel, Stepper, styled, TextField, Typography } from '@material-ui/core';
import React, { Fragment } from 'react';
import { useState } from 'react';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { taskModalInfoOpen, UpdateTaskAction } from '../../Actions/Task/taskActions';
import EditIcon from '@material-ui/icons/Edit';

const TextFieldStyled = styled(TextField)({
    marginRight: '10px',
    marginBottom: '10px'
});

function getModalStyle() {
    const top = 50;
    const left = 50;

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
        position: 'absolute',
        width: '70%',
        backgroundColor: 'transparent'
    };
}


const TaskInfoModal = () => {
    //const initials ======================================================================================================
    const [modalStyle] = useState(getModalStyle);
    const dispatch = useDispatch();
    //state redux variables ======================================================================================================
    const taskSelectedState = useSelector(state => state.task.taskSelected);

    const taskModalOpenState = useSelector(state => state.task.taskModalInfoOpen);
    //local constants ======================================================================================================

    const [activeStep, setActiveStep] = useState(0);
    const [editedTask, setEditedTask] = useState({ activities: [] });
    const [editTask, setEditTask] = useState(false);
    //functions ======================================================================================================

    //handles (react-dom) ======================================================================================================
    const handleClose = () => {
        dispatch(taskModalInfoOpen(false))
        setEditTask(false)
    };

    const onActivityFinishClick = (e) => {
        console.log('activity finished')
    };

    const handleStep = (step) => () => {
        setActiveStep(step);
    };

    const handleEdit = () => {
        setEditTask(true);
    }

    const handleCancelEditTask = () => {
        setEditTask(false)
    }

    const handleTaskEdit = (e) => {
        setEditedTask({
            ...editedTask,
            [e.target.name]: e.target.value
        })
    }
    const handleSaveEditTask = () => {
        dispatch(UpdateTaskAction(editedTask))
    }
    //effects ======================================================================================================
    useEffect(() => {
        setEditedTask(taskSelectedState)
    }, [taskSelectedState])
    return (
        <Fragment>
            <Modal
                open={taskModalOpenState}
                onClose={handleClose}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <div style={modalStyle}>
                    <Card style={{ borderRadius: '10px' }}>
                        <CardHeader
                            title="Task Information"
                            action={
                                <Fragment>
                                    <IconButton onClick={handleEdit} aria-label="settings" style={{ color: 'white' }}>
                                        <EditIcon fontSize="large" />
                                    </IconButton>
                                    <IconButton onClick={handleClose} aria-label="settings" style={{ color: 'white' }}>
                                        <HighlightOffIcon fontSize="large" />
                                    </IconButton>
                                </Fragment>

                            }
                            style={{ backgroundColor: '#32B932', color: 'white' }}
                        />
                        <CardContent style={{ boxShadow: 'inset 0px 0px 6px 0px #8C8C8C' }}>
                            <Grid container>
                                <Grid item xs={12} md={12} lg={12}>

                                </Grid>
                                <Grid item xs={12}>
                                    <Divider />
                                    <br />
                                    <div style={{ fontWeight: 'bold', marginBottom: '10px', fontSize: '18px' }} color="primary">

                                        {editTask ?
                                            <TextFieldStyled name="title" label="Title" value={editedTask.title} onChange={handleTaskEdit} />
                                            :
                                            editedTask.title
                                        }
                                    </div>
                                </Grid>
                                <Grid item sm={6} md={6} lg={2}>
                                    <TextFieldStyled name="start" label="Start Date" value={editedTask.start}
                                        InputProps={{
                                            readOnly: !editTask,
                                        }}
                                        onChange={handleTaskEdit}
                                    />
                                </Grid>
                                <Grid item sm={6} md={6} lg={2}>
                                    <TextFieldStyled name="end" label="End Date" value={editedTask.end}
                                        InputProps={{
                                            readOnly: !editTask,
                                        }}
                                        onChange={handleTaskEdit}
                                    />
                                </Grid>
                                <Grid item sm={6} lg={2}>
                                    <TextFieldStyled name="status" label="Status" value={editedTask.status}
                                        InputProps={{
                                            readOnly: !editTask,
                                        }}
                                        onChange={handleTaskEdit}
                                    />
                                </Grid>
                                <Grid item sm={12}>
                                    <Stepper activeStep={activeStep} nonLinear orientation="vertical">
                                        {editedTask.activities.map((act, index) => (
                                            <Step key={act.id}>

                                                <StepLabel onClick={handleStep(index)}>
                                                    {editTask ?
                                                        <TextFieldStyled name="titleact" label="Title" value={act.title} onChange={handleTaskEdit} />
                                                        :
                                                        act.title
                                                    }

                                                </StepLabel>
                                                <StepContent style={{ backgroundColor: '#f1f1f138' }}>
                                                    <Typography style={{ padding: '10px' }}>
                                                        {editTask ?
                                                            <TextFieldStyled name="description" label="Description" value={act.description} onChange={handleTaskEdit} />
                                                            :
                                                            act.description
                                                        }
                                                    </Typography>
                                                    <div style={{ marginTop: '10px', marginBottom: '15px' }}>
                                                        <Button
                                                            variant="contained"
                                                            color="primary"
                                                            onClick={onActivityFinishClick}
                                                        >
                                                            Finish
                                                        </Button>
                                                    </div>
                                                </StepContent>
                                            </Step>
                                        ))}
                                    </Stepper>
                                </Grid>
                            </Grid>
                        </CardContent>
                        <CardActions style={{ backgroundColor: 'antiquewhite' }}>
                            <Grid container>
                                <Grid item xs={5}>
                                    <Button
                                        variant="contained"
                                        color="success"
                                        startIcon={<SaveIcon />}
                                        style={{ float: 'left' }}
                                    >
                                        Finish Task
                                    </Button>
                                </Grid>
                                <Grid item xs={7}>
                                    {editTask ?
                                        <div>
                                            <Button
                                                variant="contained"
                                                color="secondary"
                                                startIcon={<SaveIcon />}
                                                style={{ marginRight: '20px', float: 'right' }}
                                                onClick={handleSaveEditTask}
                                            >
                                                Save
                                            </Button>
                                            <Button
                                                variant="contained"
                                                color="secondary"
                                                startIcon={<CancelIcon />}
                                                style={{ marginRight: '20px', float: 'right' }}
                                                onClick={handleCancelEditTask}
                                            >
                                                Cancel
                                            </Button>
                                        </div>
                                        :
                                        null
                                    }

                                </Grid>
                            </Grid>
                        </CardActions>
                    </Card>
                </div>
            </Modal>
        </Fragment>
    );
}

export default TaskInfoModal;