import React, { Fragment } from 'react';
import "react-big-calendar/lib/css/react-big-calendar.css";
import { useDispatch, useSelector } from 'react-redux';
import { setTaskSelectedAction, taskModalInfoOpen } from '../../Actions/Task/taskActions';
import { Avatar, Divider, List, ListItem, ListItemAvatar, ListItemText, Typography } from '@material-ui/core';
import InfoIcon from '@material-ui/icons/Info';
import WarningIcon from '@material-ui/icons/Warning';


const TasksList = () => {
    //const initials ======================================================================================================
    const dispatch = useDispatch();
    //state redux variables ======================================================================================================
    const taskListState = useSelector(state => state.task.taskList)
    //local constants ======================================================================================================

    //functions ======================================================================================================

    //handles (react-dom) ======================================================================================================
    const onClickTask = (e) => {
        dispatch(setTaskSelectedAction(e));
        dispatch(taskModalInfoOpen(true));

    }
    //effects ======================================================================================================
    return (
        <Fragment>
            <List component="nav" style={{ boxShadow: 'inset -5px 0px 5px -3px rgba(165,165,165,0.69)', height: '100%' }}>
                {taskListState.length === 0 ? <h3>No tasks</h3> : null}
                {taskListState.map((task) => (
                    <Fragment key={task.id + "frag"}>
                        <ListItem key={task.id} style={{ backgroundColor: task.status === 'Working' ? '#0088ff21' : '' }} button alignItems="flex-start" onClick={() => onClickTask(task)}>
                            <ListItemAvatar key={task.id + "avatar"}>
                                {task.priority === 'high' ?
                                    <Avatar key={task.id + "avatarWarn"} alt="Remy Sharp" style={{ backgroundColor: 'red' }}>
                                        <WarningIcon />
                                    </Avatar>
                                    : null}
                                {task.priority === 'normal' ?
                                    <Avatar key={task.id + "avatarInf"} alt="Remy Sharp" style={{ backgroundColor: '#6ebafd' }}>
                                        <InfoIcon />
                                    </Avatar>
                                    : null}
                            </ListItemAvatar>
                            <ListItemText
                                key={task.id + "text"}
                                primary={task.title}
                                secondary={
                                    <Fragment>
                                        <Typography
                                            component="span"
                                            variant="body2"
                                            color="textPrimary"
                                            key={task.id + "typ"}
                                        >
                                            {task.contact.firstName + " " + task.contact.lastName}
                                        </Typography>
                                        <label style={{ float: 'right' }}>{task.status}</label>
                                        <br />
                                        {"Start date: " + task.start.toLocaleString()}
                                        <br />
                                        {"End date: " + task.end.toLocaleString()}
                                    </Fragment>
                                }
                            />
                        </ListItem>
                        <Divider key={task.id + "divider"} variant="inset" component="li" />
                    </Fragment>
                ))}
            </List>
        </Fragment>
    );
}

export default TasksList;