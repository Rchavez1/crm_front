import React, { Fragment } from 'react';
import Layout from '../Layout/layout';
import "react-big-calendar/lib/css/react-big-calendar.css";
import TasksCalendar from './tasksCalendar';
import { Button, Grid } from '@material-ui/core';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { useHistory } from 'react-router-dom';
import TaskInfoModal from './taskInfoModal';
import TasksList from './taskList';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { GetAllTasksAction } from '../../Actions/Task/taskActions';


const ViewTasks = () => {
    //const initials ======================================================================================================
    const history = useHistory();
    const dispatch = useDispatch();
    //state redux variables ======================================================================================================

    //local constants ======================================================================================================

    //functions ======================================================================================================

    //handles (react-dom) ======================================================================================================
    const onCreateTaskClick = () => {
        history.push("createTask");
    }
    //effects ======================================================================================================
    useEffect(() => {
        dispatch(GetAllTasksAction())
    }, [])
    return (
        <Fragment>
            <Layout>
                <Grid container>
                    <Grid item xs={12} style={{ marginBottom: 15 }}>
                        <Button
                            variant="contained"
                            color="secondary"
                            startIcon={<AddCircleOutlineIcon />}
                            style={{ float: 'right' }}
                            onClick={onCreateTaskClick}
                        >
                            Task
                        </Button>
                    </Grid>
                    <Grid item xs={4}>
                        <div style={{ height: '100vh', paddingTop: '39px' }}>
                            <TasksList />
                        </div>
                    </Grid>
                    <Grid item xs={8}>
                        <div style={{ height: '100vh' }}>
                            <TasksCalendar />
                        </div>
                    </Grid>
                </Grid>
                <TaskInfoModal />


            </Layout>

        </Fragment>
    );
}

export default ViewTasks;