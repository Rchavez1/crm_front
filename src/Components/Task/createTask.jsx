import { Avatar, Divider, Fab, FormControl, Grid, IconButton, InputLabel, List, ListItem, ListItemAvatar, ListItemText, MenuItem, Select, styled, TextField } from '@material-ui/core';
import React, { Fragment, useEffect } from 'react';
import { useState } from 'react';
import Layout from '../Layout/layout';
import DateRangePickerShared from '../UI/Shared/dateRangePickerShared';
import SaveIcon from '@material-ui/icons/Save';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import { SaveNewTaskAction, SetInitialTaskStateAction, SetTaskStateAction } from '../../Actions/Task/taskActions';
import { useHistory } from 'react-router-dom';
import { GetContactsAction } from '../../Actions/Contact/contactActions';
import CreateTaskActivityModal from './createTaskActivityModal';

const TextFieldStyled = styled(TextField)({
    marginRight: '10px',
    marginBottom: '10px'
});

const CreateTask = () => {
    //const initials ======================================================================================================
    const dispatch = useDispatch();
    const history = useHistory();
    //state redux variables ======================================================================================================
    const contactsState = useSelector(state => state.contact.contactsList);
    const taskActivitiesState = useSelector(state => state.task.newTask);
    //local constants ======================================================================================================
    const [newTask, setNewTask] = useState(taskActivitiesState);
    const [createTaskActivityModalOpen, setCreateTaskActivityModalOpen] = useState(false)

    //functions ======================================================================================================

    //handles (react-dom) ======================================================================================================
    const onNewTaskChange = (e) => {
        setNewTask({
            ...newTask,
            [e.target.name]: e.target.value
        })
    }

    const onSaveTaskClick = (e) => {
        dispatch(SaveNewTaskAction(newTask))
        history.push("/viewTasks")
    }

    const handleCreateTaskActivity = () => {
        setCreateTaskActivityModalOpen(true)
    }

    const handleNewTaskBlur = () => {
        //dispatch(SetTaskStateAction(newTask))
    }
    //effects ======================================================================================================
    useEffect(() => {
        dispatch(GetContactsAction())
        setNewTask({
            start: moment().toDate(),
            end: moment().toDate(),
            title: "",
            allDay: true,
            subject: "",
            contactId: "",
            priority: '',
            activities: []
        })
    }, [])
    useEffect(() => {
        dispatch(SetTaskStateAction(newTask))
    }, [newTask])
    // useEffect(() => {
    //     setNewTask({ ...newTask, activities: taskActivitiesState.activities })
    // }, [taskActivitiesState.activities])
    return (
        <Fragment>
            <Layout>
                <Grid container>
                    <Grid item xs={12} style={{ marginBottom: '15px' }}>
                        <h3>
                            New Task
                        </h3>
                        <Divider />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Grid container>
                            <Grid item xs={12} md={12}>
                                <FormControl variant="outlined" style={{ width: '100%', marginRight: '10px', marginBottom: '10px' }}>
                                    <InputLabel>Assign To</InputLabel>
                                    <Select
                                        id="assignTo"
                                        value={newTask.contactId}
                                        onChange={onNewTaskChange}
                                        onBlur={handleNewTaskBlur}
                                        label="Assign To"
                                        name="contactId"
                                    >
                                        <MenuItem value="">
                                            <em>None</em>
                                        </MenuItem>
                                        {contactsState.map(contact => (
                                            <MenuItem value={contact.id}>{contact.firstName + " " + contact.lastName}</MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} md={3}>
                                <TextFieldStyled name="title" onChange={onNewTaskChange} id="title" label="Title" onBlur={handleNewTaskBlur} />
                            </Grid>
                            <Grid item xs={12} md={3}>
                                <FormControl variant="outlined" style={{ width: '100%', marginRight: '10px', marginBottom: '10px' }}>
                                    <InputLabel>Priority</InputLabel>
                                    <Select
                                        id="priority"
                                        value={newTask.priority}
                                        onChange={onNewTaskChange}
                                        onBlur={handleNewTaskBlur}
                                        label="Priority"
                                        name="priority"
                                    >
                                        <MenuItem value="">
                                            <em>None</em>
                                        </MenuItem>
                                        <MenuItem value={"high"}>High</MenuItem>
                                        <MenuItem value={"normal"}>Normal</MenuItem>
                                        <MenuItem value={"low"}>Low</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} md={12}>
                                <br />
                                <label>Task activities <IconButton onClick={handleCreateTaskActivity}><AddCircleOutlineIcon /></IconButton></label>
                                <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
                                    {taskActivitiesState.activities ?
                                        taskActivitiesState.activities.map((activity, index) => (
                                            <ListItem>
                                                <ListItemAvatar>
                                                    <Avatar>
                                                        {index + 1}
                                                    </Avatar>
                                                </ListItemAvatar>
                                                <ListItemText primary={activity.title} secondary={activity.description} />
                                            </ListItem>
                                        ))
                                        :
                                        null
                                    }

                                </List>
                            </Grid>
                        </Grid>

                    </Grid>
                    <Grid item xs={12} md={6} style={{ paddingLeft: '15px' }}>
                        <DateRangePickerShared object={newTask} setDates={setNewTask} onBlur={handleNewTaskBlur} />
                    </Grid>
                    <Grid item xs={12}>
                        <Fab variant="extended" style={{ position: 'absolute', right: '30px', bottom: '30px' }} onClick={onSaveTaskClick}>
                            <SaveIcon />
                            Save
                        </Fab>
                    </Grid>
                </Grid>
                <CreateTaskActivityModal open={createTaskActivityModalOpen} setOpen={setCreateTaskActivityModalOpen} />
            </Layout>
        </Fragment>
    );
}

export default CreateTask;