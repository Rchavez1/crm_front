
import './App.css';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import auth from '../src/Components/Login/auth.js';
import { Fragment } from 'react';
import { createTheme, ThemeProvider } from '@material-ui/core/styles';
import store from './store';
import { Provider } from 'react-redux';
import PublicRoutes from './Routes/public';
import PrivateRoutes from './Routes/private';

const theme = createTheme({
    palette: {
        type: 'light',
        primary: {
            main: '#32b932',
        },
        secondary: {
            main: '#000000',
        },
        success: {
            main: '#4caf50',
            contrastText: '#ffffff',
        },
    },
});

const authentication = () =>
    localStorage.getItem('jwt') ?
        (<Redirect to="/home" />) :
        <PublicRoutes />


function App() {
    return (
        <Fragment>
            <ThemeProvider theme={theme}>
                <Router>
                    <Provider store={store}>
                        <Switch>
                            {
                                auth.isAuthenticated() ?
                                    <Route component={PrivateRoutes} /> : <Route path="/" render={authentication} />
                            }
                        </Switch>
                    </Provider>
                </Router>
            </ThemeProvider>
        </Fragment>

    );
}

export default App;
