import { SET_ERROR, SET_FALSE_NOTIFICATION, SET_SUCCESS } from "../../Types/Notification";


const initialState = {
    saveSuccess: false,
    saveError: false,

}

export default function loginReducer(state = initialState, action) {

    switch (action.type) {
        case SET_SUCCESS:
            return (
                {
                    ...state,
                    saveSuccess: action.payload
                }
            )
        case SET_ERROR:
            return (
                {
                    ...state,
                    saveError: action.payload
                }
            )
        case SET_FALSE_NOTIFICATION:
            return (
                {
                    ...state,
                    saveSuccess: false,
                    saveError: false
                }
            )
        default:
            return state;
    }
}