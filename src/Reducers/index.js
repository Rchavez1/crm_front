import { combineReducers } from "redux";
import contactReducer from "./Contact/contactReducer.js";
import loginReducer from "./Login/loginReducer.js";
import taskReducer from "./Task/taskReducer.js";
import notificationReducer from "./Notification/notificationReducer.js";

export default combineReducers({
    login: loginReducer,
    task: taskReducer,
    contact: contactReducer,
    notification: notificationReducer
});