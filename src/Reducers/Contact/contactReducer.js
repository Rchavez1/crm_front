import { CREATE_CONTACT_ADDITIONAL_INFO, CREATE_CONTACT_ADDRESS, CREATE_CONTACT_SAVE, GET_ALL_CONTACTS, REMOVE_CONTACT_LOCKED, SAVE_CONTACT_ADDITIONAL_INFO, SEARCH_CONTACT_SELECTED, SET_CONTACT_EDIT_MODE, SET_CONTACT_INFO_STATE, SET_CONTACT_LOCKED, SET_EMPTY_CONTACT, UPDATE_CONTACT_ADDITIONAL_INFO, UPDATE_CONTACT_GENERAL_INFO } from "../../Types/Contact";



const initialState = {
    createContact: {
        type: '',
        title: '',
        firstName: '',
        middleName: '',
        lastName: '',
        dob: new Date().toLocaleDateString('en-CA'),
        company: '',
        nationality: '',
        language: '',
        department: '',
        weekDays: '',
        rangeHours: '',
        contactAddress: null
    },
    searchContactSelected: null,
    contactsList: [

    ],
    contactsLocked: [
    ],
    editMode: false
}

export default function contactReducer(state = initialState, action) {

    switch (action.type) {
        case CREATE_CONTACT_SAVE:
            return (
                {
                    ...state,
                    createContact: {
                        ...state.createContact,
                        id: action.payload.id,
                        type: action.payload.type,
                        title: action.payload.title,
                        firstName: action.payload.firstName,
                        middleName: action.payload.middleName,
                        lastName: action.payload.lastName,
                        dob: action.payload.dob,
                        company: action.payload.company,
                        nationality: action.payload.nationality,
                        language: action.payload.language,
                        department: action.payload.department,
                        weekDays: action.payload.weekDays,
                        rangeHours: action.payload.rangeHours,

                    }
                }
            )
        case SEARCH_CONTACT_SELECTED:
            return (
                {
                    ...state,
                    searchContactSelected: action.payload
                }
            )
        case GET_ALL_CONTACTS:
            return (
                {
                    ...state,
                    contactsList: action.payload
                }
            )
        case SET_CONTACT_LOCKED:
            return (
                {
                    ...state,
                    contactsLocked: [
                        ...state.contactsLocked,
                        action.payload
                    ]
                }
            )
        case REMOVE_CONTACT_LOCKED:
            return (
                {
                    ...state,
                    contactsLocked: action.payload,
                    searchContactSelected: null
                }
            )
        case CREATE_CONTACT_ADDRESS:
            return (
                {
                    ...state,
                    createContact: {
                        ...state.createContact,
                        contactAddress: [
                            action.payload
                        ]
                    }
                }
            )
        case SET_EMPTY_CONTACT:
            return (
                {
                    ...state,
                    createContact: {}
                }
            )
        case SET_CONTACT_INFO_STATE:
            return (
                {
                    ...state,
                    createContact: action.payload
                }
            )
        case CREATE_CONTACT_ADDITIONAL_INFO:
            return (
                {
                    ...state,
                    createContact: {
                        ...state.createContact,
                        contactAdditionalInfo: action.payload
                    }
                }
            )
        case SAVE_CONTACT_ADDITIONAL_INFO:
            return (
                {
                    ...state,
                    createContact: {
                        ...state.createContact,
                        contactAdditionalInfo: action.payload
                    }
                }
            )
        case SET_CONTACT_EDIT_MODE:
            return (
                {
                    ...state,
                    editMode: action.payload
                }
            )
        case UPDATE_CONTACT_GENERAL_INFO:
            break;
        case UPDATE_CONTACT_ADDITIONAL_INFO:
            return (
                {
                    ...state,
                    searchContactSelected: {
                        ...state.searchContactSelected,
                        contactAdditionalInfo: action.payload
                    }
                }
            )
        default:
            return state;
    }
}