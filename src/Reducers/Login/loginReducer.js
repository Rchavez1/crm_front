import {
    INIT_SESSION,
    SESSION_INIT_SUCCESSFUL,
    SESSION_INIT_ERROR,
    IS_SIGNED,
    USER_INIT
}
    from '../../Types/Login/';

const initialState = {
    loginError: false,
    existingLogin: false,
    user: {
        email: '',
        fullname: '',
        token: ''
    },
    agent: {},
    loginLoading: false
}

export default function loginReducer(state = initialState, action) {

    switch (action.type) {
        case INIT_SESSION:
            return {
                ...state,
                loginLoading: true
            }
        case SESSION_INIT_SUCCESSFUL:
            return {
                ...state,
                loginLoading: false,
                existingLogin: true,
                user: action.payload
            }
        case SESSION_INIT_ERROR:
            return {
                ...state,
                loginLoading: false,
                loginError: action.payload
            }
        case IS_SIGNED:
            return {
                ...state,
                existingLogin: action.payload
            }
        case USER_INIT:
            return {
                ...state,
                agent: [action.payload]
            }
        default:
            return state;
    }
}