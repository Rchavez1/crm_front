import moment from "moment";
import { TASK_MODAL_INFO_OPEN, SET_TASK_SELECTED, SAVE_NEW_TASK, SET_TASK_ACTIVITY_STATE, SET_TASK_STATE, SET_INITIAL_TASK_STATE, GET_TASKS } from "../../Types/Task";


const initialState = {
    taskModalInfoOpen: false,
    taskSelected: {
        start: '',
        end: '',
        createdBy: '',
        title: '',
        status: '',
        priority: '',
        activities: []
    },
    taskList: [

    ],
    newTask: {
        start: moment().toDate(),
        end: moment().toDate(),
        title: "",
        allDay: true,
        subject: "",
        contactId: "",
        priority: '',
        activities: []
    }
}

export default function taskReducer(state = initialState, action) {

    switch (action.type) {
        case TASK_MODAL_INFO_OPEN:
            return {
                ...state,
                taskModalInfoOpen: action.payload
            }
        case SET_TASK_SELECTED:
            return (
                {
                    ...state,
                    taskSelected: {
                        id: action.payload.id,
                        start: action.payload.start,
                        end: action.payload.end,
                        createdBy: action.payload,
                        title: action.payload.title,
                        status: action.payload.status,
                        priority: action.payload.priority,
                        activities: action.payload.activities,
                        contat: action.payload.contact,
                        contactId: action.payload.contactId
                    }
                }
            )
        case SAVE_NEW_TASK:
            return (
                {
                    ...state,
                    taskList:
                        [
                            ...state.taskList,
                            action.payload
                        ]
                }
            )
        case SET_TASK_ACTIVITY_STATE:
            return (
                {
                    ...state,
                    newTask: {
                        ...state.newTask,
                        activities: [
                            ...state.newTask.activities,
                            action.payload
                        ]
                    }
                }
            )
        case SET_INITIAL_TASK_STATE:
            return (
                {
                    ...state,
                    newTask: {
                        start: moment().toDate(),
                        end: moment().toDate(),
                        title: "",
                        allDay: true,
                        subject: "",
                        contactId: "",
                        priority: '',
                        activities: []
                    }
                }
            )
        case SET_TASK_STATE:
            return (
                {
                    ...state,
                    newTask: {
                        start: action.payload.start,
                        end: action.payload.end,
                        title: action.payload.title,
                        allDay: action.payload.allDay,
                        subject: action.payload.subject,
                        contactId: action.payload.contactId,
                        priority: action.payload.priority,
                        activities: [
                            ...state.newTask.activities
                        ]
                    }
                }
            )
        case GET_TASKS:
            return (
                {
                    ...state,
                    taskList: action.payload
                }
            )
        default:
            return state;
    }
}